---
features:
  secondary:
  - name: "Smarter approval resets with `patch-id` support"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html#remove-all-approvals-when-commits-are-added-to-the-source-branch'
    reporter: phikai
    stage: create
    categories:
    - 'Code Review Workflow'
    epic_url:
    - 'https://gitlab.com/groups/gitlab-org/-/epics/10249'
    description: |
      To ensure all changes are reviewed and approved, it's common to remove all approvals when new commits are added to a merge request. However, rebases also unnecessarily invalidated existing approvals, even if the rebase introduced no new changes, requiring authors to seek re-approval.

      Merge request approvals now align to a [`git-patch-id`](https://git-scm.com/docs/git-patch-id). It's a reasonably stable and reasonably unique identifier that enables smarter decisions about resetting approvals. By comparing the `patch-id` before and after the rebase, we can determine if new changes were introduced that should reset approvals and require a review.

      If you have feedback about your experiences with resets now, let us know in [issue #435870](https://gitlab.com/gitlab-org/gitlab/-/issues/435870).
