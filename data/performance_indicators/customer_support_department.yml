- name: Support Satisfaction (SSAT)
  base_path: "/handbook/support/performance-indicators/"
  definition: A measure of how satisfied our customers' are with their interaction
    with the GitLab Support team. This is based on survey responses from customers
    after each ticket is solved by the Support team using a Good or Bad rating.
  target: At or above 95%
  org: Customer Support Department
  is_key: true
  health:
    level: 2
    reasons:
    - SSAT is very good (above 93%) but target is 95%.
    - Collecting feedback from dissatisfied customers to identify trends.
    - Support Managers review trends to create changes to workflows and/or training.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/#/site/gitlabpublic/views/SupportKPIs/SSAT
- name: Manager to customer support rep ratio
  base_path: "/handbook/support/performance-indicators/"
  definition: The Manager to IC Ratio is the ratio of individual contributors to one
    manager. This data comes from Bamboo HR.
  target: The target for this metric is at or below 10:1
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - While we have an overall ratio of less than 10:1 as a department at time of
      this evaluation, we have re-balanced the larger imbalance in EMEA with new management
      in place.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/SupportKPIs/ManagertoRepRatio
- name: Service Level Agreement (SLA)
  base_path: "/handbook/support/performance-indicators/"
  definition: GitLab Support commits to an initial substantive response in a specified
    amount of time from the time the customer submits a ticket. The SLA for this first
    reply is based on each customer's Support Plan.
  target: At or above 95% to Priority Support SLAs
  org: Customer Support Department
  is_key: true
  health:
    level: 2
    reasons:
    - Inconsistent performance to a 95% target achievement.
    - Focused staffing to key time gaps with higher level of ticket SLA breaches.
    - Adjusting workflow to enable working with customers in their preferred timezone.
    - Working with the data team on understanding the inconsistent we see between
      our ZenDesk SLA data and Periscope via 2857
  urls:
  - https://about.gitlab.com/support/#service-level-agreement-sla-details
  - https://gitlab.com/gitlab-data/analytics/-/issues/2857
  sisense_data:
    chart: 6892851
    dashboard: 421422
    embed: v2
    border: false
- name: Data Privacy Requests - Service Level Agreement (SLA)
  base_path: "/handbook/support/performance-indicators/"
  definition: GitLab Support responds to Data Subject Requests in an amount of time
    required by the laws of various governing bodies. The SLA for these requests
    is determined by the legal team.
  target: At or above 95% SLAs
  org: Customer Support Department
  is_key: true
  health:
    level: 2
    reasons:
    - Inconsistent performance to 30 day resolution targets 
  urls:
    - https://about.gitlab.com/handbook/support/workflows/account_deletion_access_request_workflows.html
- name: Customer Support Margin
  base_path: "/handbook/support/performance-indicators/"
  definition: Total Support headcount and non-headcount expenses as a percent of ARR.
  target: Headcount and non-headcount expenses to be at or below 10% of ARR
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - Tracked and reported monthly, and for the year we are under our margin target
      for spend.
    - Maintain focus on spend while iterating on needs for non-headcount expense requirements.
  sisense_data:
    chart: 6032346
    dashboard: 421422
    embed: v2
    border: false
- name: Customer Wait Times
  base_path: "/handbook/support/performance-indicators/"
  definition: The following KPI tracks the ratio between the median "Resolution Time" and the total median "Customer Wait Time" per ticket.
              "Resolution Time" is defined as the amount of time between the ticket creation and the last ticket communication.
              "Customer Wait Time" is defined as the total amount of time the ticket spends in the "Open" & "New" states over the tickets life cycle.
  target: At or below 35%
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - TBD
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/CustomerWaitTimeRatio/CustomerWaitTimeRatio 
- name: Support Performance to Operating Plan
  base_path: "/handbook/support/performance-indicators/"
  definition: Tracks overall Support spend against expectations from GitLab's Operational Plan.
  target: at or within 5% of Fiscal Year Operating plan
  org: Customer Support Department
  is_key: false
  health:
    level: 0
    reasons:
    - This is a new proposed PI
    - Data is currently tracked in a spreadsheet.
- name: Support Handbook MR Rate
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-mr-rate"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/support/**` over time.
  target: 0.5
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - above target
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/HandbookMRRate/HandbookMRRateDashboard
        parameters:
          - field: Department/Division
            value: Support
- name: Support MR Rate
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: Support Department MR Rate is not directly a key performance indicator used to indicate productivity of our Support team members.
    Nonetheless, we want to track the average MR merged per team member to encourage updates to Documentation and Support 'fixes'.
    We currently count all members of the Support Department (Director, EMs, ICs) in the denominator because this is a team effort.
    The full definition of MR Rate is linked in the url section.
  target: At or above 1 MRs per Month
  org: Customer Support Department
  is_key: true
  health:
    level: 1
    reasons:
    - TBD
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MergeRequestMetrics/MRRate
        filters:
          - field: Department
            value: Customer Support
  sisense_data:
    chart: 8934827
    dashboard: 686943
    shared_dashboard: fc54cf54-e5f9-4708-9dfe-9e4c921ac79e?
    embed: v2
- name: Support Team Member Retention
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Customer Support Department
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - above target
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/N5AttritionDashboard/AttritionDashboard?:iid=1"
- name: Support Average Age of Open Positions
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-vacancy-time-to-fill"
  definition: Measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Customer Support Department
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - new metric, monitoring progress
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/ReqAging
        filters:
          - field: DPT Modified Department
            value: Customer Support
- name: Support Department Discretionary Bonus Rate
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Support Department
  is_key: false
  health:
    level: 2
    reasons:
      - Metric is new and is being monitored
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/R3DiscretionaryBonusRate
        filters:
          - field: DPT Modified Department
            value: Customer Support

- name: Support Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Support Department
  is_key: false
  health:
    level: 3
    reasons:
      - Metric is new and is being monitored
      - Between Dec 2019 and Nov 2020, we had a one time change for a group of Service Agents to Support Engineers. Due to this change, the promotion rate is higher in those months. Aug 2020 through Nov 2020 have been corrected to reflect this.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/Promo-Q
        filters:
          - field: DPT Modified Department
            value: Customer Support

